###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_EH_PID                                                           ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab.                                             ##
##                                                                                          ##  
##                   EH_PID - Alalog Heater with Current/Voltage PID control                ##
##                                                                                          ##  
##                                                                                          ##  
############################         Version: 1.6             ################################
# Author:  Miklos Boros
# Date:    21-08-2020
# Version: v1.6
# Changes: 
# 1. Removed Temperature Selection 
############################         Version: 1.5             ################################
# Author:  Miklos Boros
# Date:    12-04-2020
# Version: v1.5
# Changes: 
# 1. Removed Force mode, optimized OPI 
############################         Version: 1.4             ################################
# Author:  Miklos Boros
# Date:    27-05-2019
# Version: v1.4
# Changes: 
# 1. Added internal PID control for Current/Voltage Regulation 
############################         Version: 1.3             ################################
# Author:  Miklos Boros
# Date:    27-05-2019
# Version: v1.3
# Changes: 
# 1. Variable name unification 
############################         Version: 1.2             ################################
# Author:  Miklos Boros
# Date:    28-02-2019
# Version: v1.2
# Changes: 
# 1. Major review, 
# 2. Indent,  unit standardization
############################           Version: 1.1           ################################
# Author:  Marino Vojneski
# Date:    2018-07-09
# Version: v1.1
# Changes: 
# 1. Modified "For OPI visualisation" section: Added digital variable "FB_Keep_Setpoint".
# 2. Modified "Parameter Block" section: Added digital variable "P_Keep_Setpoint".
############################           Version: 1.0           ################################
# Author:  Miklos Boros, Marino Vojneski
# Date:    2018-06-12
# Version: v1.0
##############################################################################################



############################
#  STATUS BLOCK
############################
define_status_block()

#Operation modes
add_digital("OpMode_Auto",             PV_DESC="Operation Mode Auto",       PV_ONAM="True",                              PV_ZNAM="False")
add_digital("OpMode_Manual",           PV_DESC="Operation Mode Manual",     PV_ONAM="True",                              PV_ZNAM="False")

#Heater states
add_analog("HeaterCurrent",            "REAL",  ARCHIVE=True,                              PV_DESC="Heater Current AI",                 PV_EGU="A")
add_analog("HeaterVoltage",            "REAL",  ARCHIVE=True,                              PV_DESC="Heater Voltage AI",                 PV_EGU="V")
add_analog("HeaterPower",              "REAL",  ARCHIVE=True,                              PV_DESC="Heater Power",                      PV_EGU="W")
add_analog("HeaterSP",                 "REAL",  ARCHIVE=True,                              PV_DESC="Heater Setpoint",                   PV_EGU="W")
add_analog("HeaterMV",                 "REAL",  ARCHIVE=True,                              PV_DESC="Heater Control")
add_digital("HeatingON",                PV_DESC="Heater operating",         PV_ONAM="Heating",                           PV_ZNAM="NotHeating")


#PID states
add_analog("LMN",                      "REAL",  ARCHIVE=True,                              PV_DESC="Manipulated Value",             PV_EGU="%")
add_analog("LMN_P",                    "REAL",  ARCHIVE=True,                              PV_DESC="Manipulated Value P-Action")
add_analog("LMN_I",                    "REAL",  ARCHIVE=True,                              PV_DESC="Manipulated Value I-Action")
add_analog("LMN_D",                    "REAL",  ARCHIVE=True,                              PV_DESC="Manipulated Value D-Action")
add_analog("PID_DIF",                  "REAL",  ARCHIVE=True,                              PV_DESC="Actual PID Error/Difference")
add_analog("PV",                       "REAL",  ARCHIVE=True,                              PV_DESC="PID Process Value",             PV_EGU="%")
add_analog("MAN_SP",                   "REAL",  ARCHIVE=True,                              PV_DESC="PID Setpoint Value",            PV_EGU="%")
add_analog("PID_Cycle",                "INT",                               PV_DESC="PID cycle time",                PV_EGU="ms")

#Setpoint HMI Limit
add_analog("SPHiLim",                  "REAL" ,                             PV_DESC="Maximum for SP Spinner on HMI")
add_analog("SPLoLim",                  "REAL" ,                             PV_DESC="Minimum for SP Spinner on HMI")
add_analog("CMHiLim",                  "REAL" ,                             PV_DESC="Maximum for Current Monitor")
add_analog("CMLoLim",                  "REAL" ,                             PV_DESC="Minimum for Current Monitor")
add_analog("VMHiLim",                  "REAL" ,                             PV_DESC="Maximum for Voltage Monitor")
add_analog("VMLoLim",                  "REAL" ,                             PV_DESC="Minimum for Voltage Monitor")
add_analog("PHiLim",                   "REAL" ,                             PV_DESC="Maximum for Power Monitor")
add_analog("PLoLim",                   "REAL" ,                             PV_DESC="Minimum for Power Monitor")


#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",       PV_ONAM="InhibitManual",                     PV_ZNAM="AllowManual")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",           PV_ONAM="InhibitLocking",                    PV_ZNAM="AllowLocking")

#Interlock signals
add_digital("GroupInterlock",          PV_DESC="Group Interlock",           PV_ONAM="True",                              PV_ZNAM="False")
add_string("InterlockMsg", 39,                       PV_NAME="InterlockMsg",               PV_DESC="Interlock Message")
add_digital("HeatInterlock",  ARCHIVE=True,           PV_DESC="Heat Interlock",            PV_ONAM="True",                              PV_ZNAM="False")

#for OPI visualization
add_digital("EnableAutoBtn",           PV_DESC="Enable Auto Button",        PV_ONAM="True",                              PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",      PV_ONAM="True",                              PV_ZNAM="False")
add_digital("EnablePIDConf",           PV_DESC="Enable PID Configuration",  PV_ONAM="True",                              PV_ZNAM="False")


#Locking mechanism
add_digital("DevLocked",               PV_DESC="Device Locked",             PV_ONAM="True",                              PV_ZNAM="False")
add_analog("Faceplate_LockID",         "DINT",                              PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID",         "DINT",                              PV_DESC="Guest Lock ID")

add_digital("LatchAlarm",                                                   PV_DESC="Latching of the alarms")
add_digital("GroupAlarm",                                                   PV_DESC="Group Alarm for OPI")

#Alarm signals
add_major_alarm("Power_Discrep",       "Heater Power Discrepancy",          PV_ZNAM="NominalState")
add_major_alarm("IO_Error",            "HW IO Error",                       PV_ZNAM="NominalState")
add_major_alarm("Input_Module_Error",  "Input_Module_Error",                PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error", "Output_Module_Error",               PV_ZNAM="NominalState")

#Discrepancy
add_analog("DiscrWatt",                "REAL" ,                             PV_DESC="Discrepancy In Watt",            PV_EGU="W")
add_time("DiscrTime",                  PV_DESC="Discrepancy Time Interval")


add_major_alarm("LMN_HLIM",            "LMN_HLIM",                          PV_ZNAM="True")
add_major_alarm("LMN_LLIM",            "LMN_LLIM",                          PV_ZNAM="True")
add_major_alarm("SSTriggered","SSTriggered",                                PV_ZNAM="NominalState")

#Warning signals
add_minor_alarm("ContDeviceInMan",     "ContDeviceInMan",                   PV_ZNAM="True")


#Feedbacks
add_analog("FB_Setpoint",              "REAL" ,                             PV_DESC="FB Setpoint from HMI (SP)")
add_analog("FB_Manipulated",           "REAL" ,                             PV_DESC="FB Manipulated Value (AO)",         PV_EGU="%")
add_analog("FB_Step",                  "REAL" ,                             PV_DESC="FB Step Value for Heating/Cooling", PV_EGU="%")

#PID
add_analog("FB_Gain",                  "REAL" ,                             PV_DESC="FB Proportional gain")
add_analog("FB_TI",                    "DINT" ,                             PV_DESC="FB Integration time",           PV_EGU="sec")
add_analog("FB_TD",                    "DINT" ,                             PV_DESC="FB Derivative action time",     PV_EGU="sec")

add_analog("FB_DEADB",                 "REAL" ,                             PV_DESC="FB Deadband")
add_analog("FB_LMN_HLIM",              "REAL" ,                             PV_DESC="FB High limit for MV")
add_analog("FB_LMN_LLIM",              "REAL" ,                             PV_DESC="FB Low limit for MV")

add_digital("FB_P_ActionState",        PV_DESC="Proportional Action State")
add_digital("FB_I_ActionState",        PV_DESC="Integral Action State")
add_digital("FB_D_ActionState",        PV_DESC="Differential Action State")

#Standby Synchroniztaion
add_digital("FB_SS_State",             PV_DESC="Standby Synchroniztaion State")
add_analog("FB_SS_Value",              "REAL" ,                             PV_DESC="Standby Synchroniztaion Value", PV_EGU="%")



############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons


add_digital("Cmd_Auto",                PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              PV_DESC="CMD: Manual Mode")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")


add_digital("Cmd_ForceUnlock",         PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",             PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",           PV_DESC="CMD: Unlock Device")

add_digital("Cmd_EnablePID_Conf",      PV_DESC="CMD: Enable PID configuration from OPI")
add_digital("Cmd_DisablePID_Conf",     PV_DESC="CMD: Disable PID configuration from OPI")

add_digital("Cmd_P_ON",                PV_DESC="CMD: Enable P-Action")
add_digital("Cmd_P_OFF",               PV_DESC="CMD: Disable P-Action")

add_digital("Cmd_I_ON",                PV_DESC="CMD: Enable I-Action")
add_digital("Cmd_I_OFF",               PV_DESC="CMD: Disable I-Action")

add_digital("Cmd_D_ON",                PV_DESC="CMD: Enable D-Action")
add_digital("Cmd_D_OFF",               PV_DESC="CMD: Disable D-Action")

############################
#  PARAMETER BLOCK
############################
define_parameter_block()

#PID
add_analog("P_Gain",                   "REAL" ,                             PV_DESC="PID Proportional gain")
add_analog("P_TI",                     "DINT" ,                             PV_DESC="PID Integration time",          PV_EGU="sec")
add_analog("P_TD",                     "DINT" ,                             PV_DESC="PID Derivative action time",    PV_EGU="sec")

add_analog("P_DEADB",                  "REAL" ,                             PV_DESC="PID Deadband")
add_analog("P_LMN_HLIM",               "REAL" ,                             PV_DESC="PID High limit for MV")
add_analog("P_LMN_LLIM",               "REAL" ,                             PV_DESC="PID Low limit for MV")


#Setpoint and Manipulated value from HMI
add_analog("P_Setpoint",               "REAL" ,                             PV_DESC="Setpoint from HMI (SP)")

#Step value when pressing Cmd_Open1Step or Cmd_Close1Step
add_analog("P_Step",                   "REAL" ,                             PV_DESC="Step value for SetPoint")


#Locking mechanism
add_analog("P_Faceplate_LockID",       "DINT",                              PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID",       "DINT",                              PV_DESC="Device ID after Blockicon Open")